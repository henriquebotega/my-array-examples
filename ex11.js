const decodificar = (vlr) => {
	var posMiddle = Math.floor(vlr.length / 2);

	if (vlr.length % 2 == 0) {
		return vlr[posMiddle - 1] + vlr[posMiddle];
	} else {
		return vlr[posMiddle];
	}
};

console.log(decodificar("test")); // es
console.log(decodificar("testing")); // t
console.log(decodificar("middle")); // dd
console.log(decodificar("A")); // A
