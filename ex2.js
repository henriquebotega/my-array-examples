const array = [0, 1, 0, 3, 12];
console.log("array", array);

// FOR
var resFor = [];

for (var i = 0; i < array.length; i++) {
  if (array[i] > 0) {
    resFor.push(array[i]);
  }
}

for (var i = 0; i < array.length; i++) {
  if (array[i] == 0) {
    resFor.push(0);
  }
}

console.log("for", resFor);

// FILTER
const resDiff = array.filter(item => item > 0);
const resZero = array.filter(item => item == 0);
const resFilter = [...resDiff, resZero];
console.log("filter", resFilter);

// SORT
const resSort = array.sort().reverse();
console.log("sort", resSort);

// CHARaT
var stringExemplo = "Aprendendo JavaScript na DevMedia!";
var posicaoInicial = stringExemplo.indexOf("DevMedia");
var posicaoFinal = posicaoInicial + "DevMedia".length;
var htmlExemplo = "";

for (caractere in stringExemplo) {
  htmlExemplo += stringExemplo.charAt(caractere);

  if (caractere == posicaoInicial - 1) {
    htmlExemplo += "<b>";
  }

  if (caractere == posicaoFinal - 1) {
    htmlExemplo += "</b>";
  }
}
console.log("charAt", htmlExemplo);
