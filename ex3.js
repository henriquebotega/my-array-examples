const array = [0, 1, 0, 3, 12];
console.log("array", array);

// FOR
let resFor = 0;
for (const item of array) {
  resFor += item;
}

console.log("for", resFor);

// REDUCE
var resReduce = array.reduce((result, item) => result + item, 0);
console.log("reduce", resReduce);
