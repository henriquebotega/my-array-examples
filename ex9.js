async function getPeople() {
  const response = await fetch("https://randomuser.me/api/?results=10");
  return response.json();
}

getPeople().then(data => {
  const dados = data.results;

  const mulheres = dados.filter(e => e.gender == "female");
  console.log(mulheres);

  const infoMulheres = mulheres
    .filter(e => e.dob.age > 50)
    .map(o => {
      return {
        name: `${o.name.first} ${o.name.last}`,
        age: o.dob.age,
        email: o.email
      };
    });

  console.table(infoMulheres);
});
