const array = [0, 1, 0, 3, 8, 100];
console.log("array", array);

// SORT
var resSort = array.sort((a, b) => (a !== 0 && b == 0 ? -1 : a == 0 && b != 0 ? 1 : 0));
console.log("sort", resSort);

// FILTER
const arrayLetras = "abedejame";
console.log("array letras", arrayLetras);

var resFilter = arrayLetras
  .split("")
  .filter((e, i, f) => f.indexOf(e) == i)
  .sort()
  .join("");
console.log("filter", resFilter);

var arr = ["foo", "bar", "foo"];
var novaArr = arr.filter((a, b) => arr.indexOf(a) === b);
console.log("filter", novaArr);

const str = "This this sentence has has double words.";
str.replace(/\b(\w+)\s+\1\b/gi, '$1');