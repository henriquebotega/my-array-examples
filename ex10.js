const dot = [
	"E",
	[
		["I", ["S", "U"]],
		["A", ["R", "W"]],
	],
];
const dash = [
	"T",
	[
		["N", ["D", "K"]],
		["M", ["G", "O"]],
	],
];

const decodificar = (vlr) => {
	var ret = [];
	var retString = [];

	vlr.split("").forEach((sinal, i) => {
		if (ret.length == 0) {
			sinal == "." ? ret.push(dot) : sinal == "-" ? ret.push(dash) : ret.push(dot, dash);
		} else {
			var newRet = [];

			ret.forEach((obj) => {
				var item = obj[1];
				sinal == "." ? newRet.push(item[0]) : sinal == "-" ? newRet.push(item[1]) : newRet.push(item[0], item[1]);
			});

			ret = [...newRet];
		}
	});

	ret.forEach((obj) => {
		if (obj instanceof Array) {
			retString.push(obj[0]);
		} else {
			retString.push(obj);
		}
	});

	console.log("decodificar -> retString", retString);
	return retString;
};

decodificar("?");
decodificar("??");
decodificar("???");

decodificar("."); // E
decodificar("-"); // T
decodificar("?"); // E, T
decodificar(".."); // I
decodificar(".-"); // A
decodificar("..."); // S
decodificar("..-"); // U
decodificar(".-."); // R
decodificar(".--"); // W

// K
decodificar("-.-");
// S
decodificar("...");
// A
decodificar(".-");
// M
decodificar("--");
// N
decodificar("-.");

// I, N
decodificar("?.");
// U, W, K, O
decodificar("??-");
