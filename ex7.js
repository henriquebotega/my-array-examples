const array = ["anderson", "item[3]", "posicao_item[5]"];
console.log("array", array);

// REGEX
var resRegex = /([a-zA-Z_]\[([0-9])\])/;
var testando = array.filter(i => {
  if (i.match(resRegex)) {
    return i;
  }
});
console.log("regex", testando);

const paragraph = "The quick brown fox jumps over the lazy dog. It barked.";
const regex = /[A-Z]/g;
const found = paragraph.match(regex);
console.log("found", found);

// https://javascript.info/regexp-methods
