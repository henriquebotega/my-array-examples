const array = [
  {
    id: 1,
    name: "Jonathon Haley",
    username: "Monte.Weber2",
    email: "Daphne43@yahoo.com",
    phone: "1-563-675-1857 x11708",
    website: "carmela.net",
    password: "hashed_password"
  },
  {
    id: 2,
    name: "Dean John",
    username: "dd.1",
    email: "deno@google.com",
    phone: "1-123-543-1857 123212",
    website: "dd.net",
    password: "Dean_hashed_password"
  }
];

const newItem = {
  id: 4,
  name: "Denomer Crazy",
  username: "crazy.1",
  email: "deno@crazy.com",
  phone: "",
  website: "crazy.app",
  password: "crazed_checker"
};

console.log("array", array);

// add element at last
const newArray1 = [...array, newItem];

// add element at first
const newArray2 = [newItem, ...array];

// the old way
const newArray3 = array.concat(newItem);
console.log("newArray", newArray1, newArray2, newArray3);

// this will add hobbies to users array and return newUsers array
const hobbies = ["chess", "pool"];
const newUsers = array.map(u => ({ ...u, hobbies }));
console.log("map hobbies", newUsers);

const contactInfo = array.map(({ email, website, phone }) => ({ email, website, phone }));
console.log("map info", contactInfo);

// this will return newUsers with all user having name 'te'
const newUsersTE = array.map(u => (u.id == 2 ? { ...u, name: "te" } : u));
console.log("map te", newUsersTE);

// will return an array with all keys other than website
const newUsersKeys = array.map(({ id, email, name, username, phone, password }) => ({ id, email, username, name, phone, password }));
console.log("map keys", newUsersKeys);

// Above code seems to be practically difficult to code for big objects.
const newUsersBig = array.map(u => {
  return Object.keys(u).reduce((newObj, key) => {
    return key != "website" ? { ...newObj, [key]: u[key] } : newObj;
  }, {});
});
console.log("map big", newUsersBig);
