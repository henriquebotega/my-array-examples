const decodificar = (vlr) => {
	vlr = vlr.trim();

	if (vlr.indexOf("# ") > -1) {
		var pos = vlr.indexOf("# ");

		var hSize = pos + 1;
		var texto = vlr.substr(hSize).trim();

		if (hSize < 7) {
			return `<h${hSize}>${texto}</h${hSize}>`;
		} else {
			return vlr;
		}
	} else {
		return vlr;
	}
};

console.log(decodificar("#### header ")); // <h1>header</h1>
console.log(decodificar("# header ")); // <h1>header</h1>
console.log(decodificar(" hoje ")); // hoje
console.log(decodificar("##  small header")); // <h2>small header</h2>
console.log(decodificar("###three header")); // ###three header
console.log(decodificar("#### four header")); // <h4>four header</h4>
console.log(decodificar("#Invalid")); // #Invalid
console.log(decodificar("######### Texto")); // #Invalid
