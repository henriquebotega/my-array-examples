const decodificar = (vlr) => {
	var res = 0;

	for (var i = 1; i < vlr; i++) {
		if (i % 3 === 0 || i % 5 === 0) {
			res += i;
		}
	}

	return res;
};

console.log(decodificar(10)); // 23
