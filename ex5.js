var users = [
  { id: 12, name: "Baze Malbus", years: 14, pilot: false },
  { id: 44, name: "Bodhi Rook", years: 62, pilot: true },
  { id: 59, name: "Chirrut Îmwe", years: 24, pilot: false },
  { id: 122, name: "Jyn Erso", years: 41, pilot: true }
];

console.log("users", users);

// FOREACH
var resForeach = false;

users.forEach(function(operative) {
  if (operative.pilot) {
    resForeach = true;
  }
});

console.log("foreach", resForeach);

// SOME
const resSome = users.some(operative => operative.pilot);
console.log("some", resSome);

// FIND
const resFind = users.find(operative => operative.pilot);
console.log("find", resFind);

// EVERY
const isBelowThreshold = currentValue => currentValue < 40;
const array1 = [1, 30, 39, 29, 10, 13];
var resEvery = array1.every(isBelowThreshold);
console.log("every", resEvery);

var ages = [32, 33, 15, 40];
var resEveryOver18 = ages.every(currentValue => currentValue > 18);
console.log("every 18", resEveryOver18);

// FILTER
const words = ["spray", "limit", "elite", "exuberant", "destruction", "present"];
const resFilter = words.filter(word => word.length > 6);
console.log("filter", resFilter);

// REDUCE
var resReduce = users.reduce((oldest, pilot) => ((oldest.years || 0) > pilot.years ? oldest : pilot));
console.log("reduce", resReduce);

var personnel = [
  {
    id: 5,
    name: "Luke Skywalker",
    pilotingScore: 98,
    shootingScore: 56,
    isForceUser: true
  },
  {
    id: 82,
    name: "Sabine Wren",
    pilotingScore: 73,
    shootingScore: 99,
    isForceUser: false
  },
  {
    id: 22,
    name: "Zeb Orellios",
    pilotingScore: 20,
    shootingScore: 59,
    isForceUser: false
  },
  {
    id: 15,
    name: "Ezra Bridger",
    pilotingScore: 43,
    shootingScore: 67,
    isForceUser: true
  },
  {
    id: 11,
    name: "Caleb Dume",
    pilotingScore: 71,
    shootingScore: 85,
    isForceUser: true
  }
];

console.log("personnel", personnel);

var jediPersonnel = personnel.filter(function(person) {
  return person.isForceUser;
});
console.log("filter", jediPersonnel);

var jediScores = jediPersonnel.map(function(jedi) {
  return jedi.pilotingScore + jedi.shootingScore;
});
console.log("map", jediScores);

var totalJediScore = jediScores.reduce(function(acc, score) {
  return acc + score;
}, 0);
console.log("reduce", totalJediScore);

var total3em1 = personnel
  .filter(function(person) {
    return person.isForceUser;
  })
  .map(function(jedi) {
    return jedi.pilotingScore + jedi.shootingScore;
  })
  .reduce(function(acc, score) {
    return acc + score;
  }, 0);
console.log("3 em 1", total3em1);

const total3em1Reduzido = personnel
  .filter(person => person.isForceUser)
  .map(jedi => jedi.pilotingScore + jedi.shootingScore)
  .reduce((acc, score) => acc + score, 0);
console.log("3 em 1 reduzido", total3em1Reduzido);

const filterItems = letters => {
  return personnel.filter(item => item["name"].indexOf(letters) > -1);
};
console.log("filter", filterItems("al"));
