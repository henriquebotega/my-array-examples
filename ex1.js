const array = [0, 1, 0, 3, 8, 100];
console.log("array", array);

// SOME
// returns only 'true' for your expression
var resSome = array.some(obj => obj > 100);
console.log("some", resSome);

// MAP
// return a new array with all value as multiple of 10;
const resMap = array.map(i => i * 10);
console.log("map", resMap);

// REDUCE
// logic to perform to get accumulator as a return value
const resReduce = array.reduce((acc, item) => (acc += item), 0);
console.log("reduce", resReduce);

// FILTER
// return a new array with value [4, 12]
const resFilter = array.filter(i => i > 0 && i % 2 == 0);
console.log("filter", resFilter);
