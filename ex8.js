const meuObj = [
  { name: "anderson", idade: 38 },
  { name: "henrique", idade: 5 },
  { name: "paula", idade: 17 },
  { name: "botega", idade: 5 },
  { name: "angelica", idade: 11 }
];

console.log("meuObj", meuObj);

// Exibir somente a idade, sem repetir, e menor que 18
// [5, 11, 17]

// Executa o 'filter' para retornar somente menores de idade
// Depois, executa o map, para retornar somente o atributo idade
var menorDeIdade = meuObj.filter(i => i.idade < 18).map(o => o.idade);
console.log("menorDeIdade", menorDeIdade);

// Executa o filter e verifica se existem itens semelhantes
var semRepetir = menorDeIdade.filter((e, i, f) => f.indexOf(e) == i);
console.log("semRepetir", semRepetir);

// Executa o 'sort' verificando se o item posterior eh menor que o anterior
var colocarOrdem = semRepetir.sort((a, b) => a - b);
console.log("colocarOrdem", colocarOrdem);

/* OBJETOS */

// Executa o 'filter' para retornar os registros somente menores de idade
var objMenorDeIdade = meuObj.filter(i => i.idade < 18);
console.log("objMenorDeIdade", objMenorDeIdade);

// Executa o 'map'
var objSemRepetir = [];

objMenorDeIdade.forEach(i => {
  if (!objSemRepetir.some(obj => obj["idade"] == i["idade"])) {
    objSemRepetir.push(i);
  }
});

console.log("objSemRepetir", objSemRepetir);

// Executa o 'sort' verificando se o item posterior eh menor que o anterior
var objColocarOrdem = objSemRepetir.sort((a, b) => a["idade"] - b["idade"]);
console.log("objColocarOrdem", objColocarOrdem);

var array_date = [
  ["14:00", "15:00"],
  ["11:00", "11:30"],
  ["15:00", "19:30"]
];

var compararHoras = (horario1, horario2) => {
  var [h1, m1] = horario1.split(":");
  var [h2, m2] = horario2.split(":");

  var t1 = parseInt(h1) * 60 + parseInt(m1);
  var t2 = parseInt(h2) * 60 + parseInt(m2);

  var tt = (t2 - t1) * 60;

  var h = parseInt(tt / 3600);
  h = h < 10 ? "0" + h : h;
  var m = parseInt((tt % 3600) / 60);
  m = m < 10 ? "0" + m : m;
  // var s = parseInt(tt % 60)
  // s = s < 10 ? "0" + s : s;

  return h + ":" + m;
};

array_date.forEach(e => {
  console.log("tempo de reuniao", compararHoras(e[0], e[1]));
});
